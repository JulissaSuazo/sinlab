﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SINLAB.Startup))]
namespace SINLAB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
