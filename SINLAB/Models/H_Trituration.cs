﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class H_Trituration
    {
        [key]
        public int H_TriturationId { get; set; }

        [Display(Name ="Fecha")]
        [Required(ErrorMessage ="Debe Ingresar {0}")]
        public DateTime Date { get; set; }


    }
}