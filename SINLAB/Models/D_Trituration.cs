﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class D_Trituration
    {   
        public int H_TriturationId { get; set; }

        public int Correlative { get; set; }
        public int ElementId { get; set; }
        public decimal Value { get; set; }


    }
}