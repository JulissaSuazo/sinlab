﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class SINLABContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SINLABContext() : base("name=SINLABContext")
        {

        }

        public System.Data.Entity.DbSet<SINLAB.Models.Oven> Ovens { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.Crusher> Crushers { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.TypeMill> TypeMills { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.Mill> Mills { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.Silo> Silos { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.Element> Elements { get; set; }

        public System.Data.Entity.DbSet<SINLAB.Models.Prehomo> Prehomoes { get; set; }
    }
}
