﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class Oven
    {
        [Key]
        public int OvenId { get; set; }

        [Display(Name ="Codigo")]
        [Required(ErrorMessage ="Debe Ingresar {0}")]
        public string Code { get; set; }

        [Display(Name = "Descripción ")]
        [Required(ErrorMessage = "Debe Ingresar {0}")]
        public string Description { get; set; }
    }
}