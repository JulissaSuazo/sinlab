﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class Silo
    {
        [Key]
        public int SiloId { get; set; }

        [Display(Name = "Descripción ")]
        [Required(ErrorMessage ="You must be {0}")]
        public string Description { get; set; }
    }
}