﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class TypeMill
    {
        [Key]public int TypeMillId { get; set; }

        [Display(Name = "Descripción ")]
        [Required(ErrorMessage = "Debe Ingresar{0}")]
        public string Description { get; set; }

      
       public virtual ICollection<Mill> Mills { get; set; }
    }
}