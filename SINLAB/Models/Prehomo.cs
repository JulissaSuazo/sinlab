﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SINLAB.Models
{
    public class Prehomo
    {
        [key]
        public int PrehomoId { get; set; }

        [Display(Name = "Descripción ")]
        [Required(ErrorMessage ="Debe Ingresar {0}")]
        public string Description { get; set; }
    }
}