﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class OvensController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: Ovens
        public ActionResult Index()
        {
            return View(db.Ovens.ToList());
        }

        // GET: Ovens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oven oven = db.Ovens.Find(id);
            if (oven == null)
            {
                return HttpNotFound();
            }
            return View(oven);
        }

        // GET: Ovens/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ovens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OvenId,Code,Description")] Oven oven)
        {
            if (ModelState.IsValid)
            {
                db.Ovens.Add(oven);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oven);
        }

        // GET: Ovens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oven oven = db.Ovens.Find(id);
            if (oven == null)
            {
                return HttpNotFound();
            }
            return View(oven);
        }

        // POST: Ovens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OvenId,Code,Description")] Oven oven)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oven).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oven);
        }

        // GET: Ovens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oven oven = db.Ovens.Find(id);
            if (oven == null)
            {
                return HttpNotFound();
            }
            return View(oven);
        }

        // POST: Ovens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oven oven = db.Ovens.Find(id);
            db.Ovens.Remove(oven);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
