﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class PrehomoesController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: Prehomoes
        public ActionResult Index()
        {
            return View(db.Prehomoes.ToList());
        }

        // GET: Prehomoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prehomo prehomo = db.Prehomoes.Find(id);
            if (prehomo == null)
            {
                return HttpNotFound();
            }
            return View(prehomo);
        }

        // GET: Prehomoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Prehomoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PrehomoId,Description")] Prehomo prehomo)
        {
            if (ModelState.IsValid)
            {
                db.Prehomoes.Add(prehomo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(prehomo);
        }

        // GET: Prehomoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prehomo prehomo = db.Prehomoes.Find(id);
            if (prehomo == null)
            {
                return HttpNotFound();
            }
            return View(prehomo);
        }

        // POST: Prehomoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PrehomoId,Description")] Prehomo prehomo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prehomo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(prehomo);
        }

        // GET: Prehomoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prehomo prehomo = db.Prehomoes.Find(id);
            if (prehomo == null)
            {
                return HttpNotFound();
            }
            return View(prehomo);
        }

        // POST: Prehomoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Prehomo prehomo = db.Prehomoes.Find(id);
            db.Prehomoes.Remove(prehomo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
