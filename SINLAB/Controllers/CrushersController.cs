﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class CrushersController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: Crushers
        public ActionResult Index()
        {
            return View(db.Crushers.ToList());
        }

        // GET: Crushers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crusher crusher = db.Crushers.Find(id);
            if (crusher == null)
            {
                return HttpNotFound();
            }
            return View(crusher);
        }

        // GET: Crushers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Crushers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CrusherId,Code,Description")] Crusher crusher)
        {
            if (ModelState.IsValid)
            {
                db.Crushers.Add(crusher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(crusher);
        }

        // GET: Crushers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crusher crusher = db.Crushers.Find(id);
            if (crusher == null)
            {
                return HttpNotFound();
            }
            return View(crusher);
        }

        // POST: Crushers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CrusherId,Code,Description")] Crusher crusher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(crusher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(crusher);
        }

        // GET: Crushers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Crusher crusher = db.Crushers.Find(id);
            if (crusher == null)
            {
                return HttpNotFound();
            }
            return View(crusher);
        }

        // POST: Crushers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Crusher crusher = db.Crushers.Find(id);
            db.Crushers.Remove(crusher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
