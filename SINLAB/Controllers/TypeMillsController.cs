﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class TypeMillsController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: TypeMills
        public ActionResult Index()
        {
            return View(db.TypeMills.ToList());
        }

        // GET: TypeMills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeMill typeMill = db.TypeMills.Find(id);
            if (typeMill == null)
            {
                return HttpNotFound();
            }
            return View(typeMill);
        }

        // GET: TypeMills/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeMills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TypeMillId,Description")] TypeMill typeMill)
        {
            if (ModelState.IsValid)
            {
                db.TypeMills.Add(typeMill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeMill);
        }

        // GET: TypeMills/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeMill typeMill = db.TypeMills.Find(id);
            if (typeMill == null)
            {
                return HttpNotFound();
            }
            return View(typeMill);
        }

        // POST: TypeMills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TypeMillId,Description")] TypeMill typeMill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeMill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeMill);
        }

        // GET: TypeMills/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeMill typeMill = db.TypeMills.Find(id);
            if (typeMill == null)
            {
                return HttpNotFound();
            }
            return View(typeMill);
        }

        // POST: TypeMills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeMill typeMill = db.TypeMills.Find(id);
            db.TypeMills.Remove(typeMill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
