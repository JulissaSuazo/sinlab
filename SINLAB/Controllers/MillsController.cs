﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class MillsController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: Mills
        public ActionResult Index()
        {
            var mills = db.Mills.Include(m => m.TypeMill);
            return View(mills.ToList());
        }

        // GET: Mills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mill mill = db.Mills.Find(id);
            if (mill == null)
            {
                return HttpNotFound();
            }
            return View(mill);
        }

        // GET: Mills/Create
        public ActionResult Create()
        {
            ViewBag.TypeMillId = new SelectList(db.TypeMills, "TypeMillId", "Description");
            return View();
        }

        // POST: Mills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MillId,Code,Description,TypeMillId")] Mill mill)
        {
            if (ModelState.IsValid)
            {
                db.Mills.Add(mill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TypeMillId = new SelectList(db.TypeMills, "TypeMillId", "Description", mill.TypeMillId);
            return View(mill);
        }

        // GET: Mills/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mill mill = db.Mills.Find(id);
            if (mill == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeMillId = new SelectList(db.TypeMills, "TypeMillId", "Description", mill.TypeMillId);
            return View(mill);
        }

        // POST: Mills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MillId,Code,Description,TypeMillId")] Mill mill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TypeMillId = new SelectList(db.TypeMills, "TypeMillId", "Description", mill.TypeMillId);
            return View(mill);
        }

        // GET: Mills/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mill mill = db.Mills.Find(id);
            if (mill == null)
            {
                return HttpNotFound();
            }
            return View(mill);
        }

        // POST: Mills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mill mill = db.Mills.Find(id);
            db.Mills.Remove(mill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
