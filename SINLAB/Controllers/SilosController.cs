﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SINLAB.Models;

namespace SINLAB.Controllers
{
    public class SilosController : Controller
    {
        private SINLABContext db = new SINLABContext();

        // GET: Silos
        public ActionResult Index()
        {
            return View(db.Silos.ToList());
        }

        // GET: Silos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silo silo = db.Silos.Find(id);
            if (silo == null)
            {
                return HttpNotFound();
            }
            return View(silo);
        }

        // GET: Silos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Silos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SiloId,Description")] Silo silo)
        {
            if (ModelState.IsValid)
            {
                db.Silos.Add(silo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(silo);
        }

        // GET: Silos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silo silo = db.Silos.Find(id);
            if (silo == null)
            {
                return HttpNotFound();
            }
            return View(silo);
        }

        // POST: Silos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SiloId,Description")] Silo silo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(silo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(silo);
        }

        // GET: Silos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silo silo = db.Silos.Find(id);
            if (silo == null)
            {
                return HttpNotFound();
            }
            return View(silo);
        }

        // POST: Silos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Silo silo = db.Silos.Find(id);
            db.Silos.Remove(silo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
